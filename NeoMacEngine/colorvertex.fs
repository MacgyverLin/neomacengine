#version 330 core
in vec4 vColor;
out vec4 FragColor;
uniform vec4 color;
void main()
{
    FragColor = vColor * color;
}