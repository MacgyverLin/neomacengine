#include "Texture.h"

#define STB_IMAGE_IMPLEMENTATION
#include "stb_image.h"


bool Texture::construct(const char* filename_, Format format_, bool mipmap_, int bufferType_)
{
	int width, height, nrChannels;
	unsigned char* data = stbi_load(filename_, &width, &height, &nrChannels, 0);
	if (data)
	{
		bool success = construct(data, width, height, format_, mipmap_, bufferType_);

		stbi_image_free(data);

		return success;
	}
	else
	{
		return false;
	}
}