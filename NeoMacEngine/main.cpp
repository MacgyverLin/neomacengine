#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "NeoMacEngine.h"
#include "TestScene.h"

TestScene test_scene;
Scene* scene = &test_scene;

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
bool ServiceCreate(GLFWwindow* window);
bool ServiceProcess(GLFWwindow* window);
void ServiceDestroy(GLFWwindow* window);

int main()
{
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	//glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);

	GLFWwindow* window = glfwCreateWindow(800, 600, "Test", NULL, NULL);
	if (window == NULL)
	{
		std::cout << "Failed to create GLFW window" << std::endl;
		glfwTerminate();
		return -1;
	}
	glfwMakeContextCurrent(window);
	glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);

	if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
	{
		std::cout << "Failed to initialize GLAD" << std::endl;
		return -1;
	}

	if (ServiceCreate(window))
	{
		bool done = false;
		while (!glfwWindowShouldClose(window) && !done)
		{
			done = ServiceProcess(window);

			glfwSwapBuffers(window);
			glfwPollEvents();
		}

		ServiceDestroy(window);
	}

	glfwTerminate();
	return 0;
}

bool ServiceCreate(GLFWwindow* window)
{
	if (!scene->Init())
		return false;

	return true;
}

bool RenderProcess(GLFWwindow* window)
{
	scene->Process();

	return false;
}

bool InputProcess(GLFWwindow* window)
{
	if (glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
		glfwSetWindowShouldClose(window, true);

	if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
		Input::dx = -0.1f;
	if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
		Input::dx = +0.1f;
	if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
		Input::dz = -0.1f;
	if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
		Input::dz = +0.1f;
	if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
		Input::dy = +0.1f;
	if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
		Input::dy = -0.1f;

	glfwGetCursorPos(window, &Input::mouseX, &Input::mouseY);

	return false;
}

bool ServiceProcess(GLFWwindow* window)
{
	bool done = false;

	done = RenderProcess(window);
	done = InputProcess(window);

	return done;
}

void ServiceDestroy(GLFWwindow* window)
{
	scene->DeInit();
}

void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
	glViewport(0, 0, width, height);
}