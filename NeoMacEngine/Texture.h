#ifndef _Texture_h_
#define _Texture_h_

#include "Graphics.h"

class Texture
{
public:
    enum Format
    {
        RGBA8888,
        RGBA4444,
        RGB888,
        RGB565,
        A8,
        L8,
        L8A8,
    };

    Texture(GLenum type_)
        : type(type_)
        , tbo(0)
        , wrapS(GL_REPEAT)
        , wrapT(GL_REPEAT)
        , wrapR(GL_REPEAT)
        , minFilter(GL_LINEAR)
        , magFilter(GL_LINEAR_MIPMAP_LINEAR)
    {
    }

    ~Texture()
    {
        destruct();
    }

    bool construct(const char* filename_, Format format_, bool mipmap_ = true, int bufferType_ = GL_STATIC_DRAW);

    bool construct(const unsigned char* data_, unsigned int width_, unsigned int height_, Format format_, bool mipmap_ = true, int bufferType_ = GL_STATIC_DRAW)
    {
        destruct();

        glGenTextures(1, &tbo);
        if (tbo)
        {
            glBindTexture(type, tbo);

            switch (format_)
            {
                case RGBA8888:
                    setTexImage(type, 0, GL_RGBA, width_, height_, 0, GL_RGBA, GL_UNSIGNED_BYTE, data_);
                    break;
                case RGBA4444:
                    setTexImage(type, 0, GL_RGBA, width_, height_, 0, GL_RGBA, GL_UNSIGNED_SHORT_4_4_4_4, data_);
                    break;
                case RGB888:
                    setTexImage(type, 0, GL_RGB, width_, height_, 0, GL_RGB, GL_UNSIGNED_BYTE, data_);
                    break;
                case RGB565:
                    setTexImage(type, 0, GL_RGB, width_, height_, 0, GL_RGB, GL_UNSIGNED_SHORT_5_6_5, data_);
                    break;
                case A8:
                    setTexImage(type, 0, GL_ALPHA, width_, height_, 0, GL_ALPHA, GL_UNSIGNED_BYTE, data_);
                    break;
                case L8:
                    setTexImage(type, 0, GL_LUMINANCE, width_, height_, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, data_);
                    break;
                case L8A8:
                    setTexImage(type, 0, GL_LUMINANCE_ALPHA, width_, height_, 0, GL_LUMINANCE_ALPHA, GL_UNSIGNED_BYTE, data_);
                    break;
            };

            if(mipmap_)
                glGenerateMipmap(type);

            return true;
        }
        else
        {
            return false;
        }
    }

    void destruct()
    {
        if (tbo)
        {
            glDeleteTextures(1, &tbo);
            tbo = 0;
        }
    }

    unsigned int getType() const
    {
        return type;
    }

    void setWrapS(int wrapS_)
    {
        wrapS = wrapS_;
    }

    void setWrapT(int wrapT_)
    {
        wrapT = wrapT_;
    }

    void setWrapR(int wrapR_)
    {
        wrapR = wrapR_;
    }

    void setMinFilter(int minFilter_)
    {
        minFilter = minFilter_;
    }

    void setMagFilter(int magFilter_)
    {
        magFilter = magFilter_;
    }

    unsigned int getWrapS() const
    {
        return wrapS;
    }

    unsigned int getWrapT() const
    {
        return wrapT;
    }

    unsigned int getWrapR() const
    {
        return wrapR;
    }

    unsigned int getMinFilter() const
    {
        return minFilter;
    }

    unsigned int getMagFilter() const
    {
        return magFilter;
    }

    void apply(unsigned int channel) const
    {
        if (tbo)
        {
            glActiveTexture(channel);
            glBindTexture(type, tbo);
            glTexParameteri(type, GL_TEXTURE_WRAP_S, wrapS);
            glTexParameteri(type, GL_TEXTURE_WRAP_T, wrapT);
            glTexParameteri(type, GL_TEXTURE_WRAP_R, wrapR);
            glTexParameteri(type, GL_TEXTURE_MIN_FILTER, minFilter);
            glTexParameteri(type, GL_TEXTURE_MAG_FILTER, magFilter);
        }
    }
protected:
    virtual void setTexImage(GLenum target,
        GLint level,
        GLint internalFormat,
        GLsizei width,
        GLsizei height,
        GLint border,
        GLenum format,
        GLenum type,
        const GLvoid* data) = 0;
private:


public:
protected:
private:
    GLenum type;
    GLuint tbo;
    GLint wrapS;
    GLint wrapT;
    GLint wrapR;
    GLint minFilter;
    GLint magFilter;
};

#endif