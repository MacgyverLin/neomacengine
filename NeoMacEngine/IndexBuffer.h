#ifndef _IndexBuffer_h_
#define _IndexBuffer_h_

#include <vector>
#include <functional>
#include "Graphics.h"
#include "VertexAttribute.h"

template<class Index>
class IndexBuffer
{
public:
    IndexBuffer()
        : ibo(0)
        , count(0)
    {
    }

    ~IndexBuffer()
    {
        destruct();
    }

    bool construct(const Index* indices_, int indexCount_, int bufferType_ = GL_STATIC_DRAW)
    {
        destruct();

        glGenBuffers(1, &ibo);
        if (ibo)
        {
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
            glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Index) * indexCount_, indices_, bufferType_);

            count = indexCount_;

            return true;
        }
        else
        {
            return false;
        }
    }

    void destruct()
    {
        if (ibo)
        {
            glDeleteBuffers(1, &ibo);
            ibo = 0;
        }

        count = 0;
    }

    unsigned int getPrimitiveCount() const
    {
        return count;
    }

    void draw(GLenum mode, unsigned int start, unsigned int count)
    {
        glDrawElements(mode, count, GL_UNSIGNED_INT, (const void*)0);
    }
protected:
private:


public:
protected:
private:
    GLuint ibo;
    int count;
};

#endif