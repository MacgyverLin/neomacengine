#ifndef _VertexAttribute_h_
#define _VertexAttribute_h_

#include <vector>
#include <functional>
#include "Graphics.h"

struct Layout
{
    unsigned int index;
    unsigned int size;
    unsigned int type;
    bool normalized;
    unsigned int stride;
};

class VertexAttribute
{
public:
    VertexAttribute()
        : vao(0)
    {
    }

    ~VertexAttribute()
    {
        destruct();
    }

    bool construct(const std::vector<Layout>& layouts_, std::function<void()> cb)
    {
        destruct();

        glGenVertexArrays(1, &vao);
        if (vao)
        {
            layouts = layouts_;
            glBindVertexArray(vao);

            cb();

            unsigned long long offset = 0;
            for (auto& layout : layouts)
            {
                glVertexAttribPointer(layout.index, layout.size, layout.type, layout.normalized, layout.stride, (void*)offset);
                glEnableVertexAttribArray(layout.index);

                unsigned int numByte = 0;
                if (layout.type == GL_BYTE || layout.type == GL_UNSIGNED_BYTE)
                    numByte = 1;
                else if (layout.type == GL_SHORT || layout.type == GL_UNSIGNED_SHORT)
                    numByte = 2;
                else if (layout.type == GL_INT || layout.type == GL_UNSIGNED_INT)
                    numByte = 4;
                else if (layout.type == GL_FLOAT)
                    numByte = 4;
                else if (layout.type == GL_DOUBLE)
                    numByte = 8;

                offset += numByte * layout.size;
            }

            glBindVertexArray(0);

            return true;
        }
        else
            return false;
    }

    void destruct()
    {
        if (vao)
        {
            glDeleteVertexArrays(1, &vao);
            vao = 0;

            layouts.clear();
        }
    }

    void apply()
    {
        if (vao)
        {
            glBindVertexArray(vao);
        }
    }
protected:
private:


public:
protected:
    std::vector<Layout> layouts;
    GLuint vao;
private:
};

#endif