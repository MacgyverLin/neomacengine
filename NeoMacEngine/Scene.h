#ifndef _Scene_h_
#define _Scene_h_

class Scene
{
public:
	Scene()
	{
	}

	virtual ~Scene()
	{
	}

	virtual bool Init() = 0;
	virtual void Process() = 0;
	virtual void DeInit() = 0;
};

#endif