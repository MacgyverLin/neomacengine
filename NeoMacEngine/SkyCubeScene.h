#ifndef _TestScene_h_
#define _TestScene_h_

#include "NeoMacEngine.h"

class TestScene : public Scene
{
public:
	TestScene()
	{
	}

	~TestScene()
	{
	}

	bool Init()
	{
		if (!InitCamera())
			return false;

		if (!InitTextures())
			return false;

		if (!InitShaders())
			return false;

		if (!InitMeshes())
			return false;

		return true;
	}

	void getMouseDeltaX(double& deltaX, double& deltaY)
	{
		static double old_mousex = 0.0, old_mousey = 0.0;
		static bool first_time = true;
		if (first_time)
		{
			old_mousex = Input::mouseX;
			old_mousey = Input::mouseY;
			first_time = false;
		}

		double mousex, mousey;
		mousex = Input::mouseX;
		mousey = Input::mouseY;

		deltaX = mousex - old_mousex;
		deltaY = mousey - old_mousey;
		old_mousex = mousex;
		old_mousey = mousey;
	}

	void Process()
	{
		double deltaX;
		double deltaY;
		getMouseDeltaX(deltaX, deltaY);
		UpdateCamera(deltaX, deltaY, Input::dx, Input::dy, Input::dz);

		float t = (float)glfwGetTime();
		float r = sin(t) / 1.0f + 0.5f;
		float g = sin(t / 2.0f) / 2.0f + 0.5f;
		float b = sin(t / 4.0f) / 2.0f + 0.5f;

		camera.draw();
		glEnable(GL_CULL_FACE);
		glCullFace(GL_BACK);

		glEnable(GL_DEPTH_TEST);
		glDepthFunc(GL_LEQUAL);

		vertexMesh.setTranslate(glm::vec3(-0.25, 0.0, -0.75));
		vertexShader.use();
		vertexShader.setUniform4f("color", 1.0f, 0.0f, 0.0f, 1.0f);
		vertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(vertexMesh.getGlobalTransform()));
		vertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		vertexMesh.draw();

		vertexIndexMesh.setTranslate(glm::vec3(0.25, 0.0, -0.75));
		vertexShader.use();
		vertexShader.setUniform4f("color", 0.0f, g, 0.0f, 1.0f);
		vertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(vertexIndexMesh.getGlobalTransform()));
		vertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		vertexIndexMesh.draw();

		colorVertexMesh.setTranslate(glm::vec3(-0.25, 0.0, -0.25));
		colorVertexShader.use();
		colorVertexShader.setUniform4f("color", 0.2f, 0.5f, 0.5f, 1.0f);
		colorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(colorVertexMesh.getGlobalTransform()));
		colorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		colorVertexMesh.draw();

		colorVertexIndexMesh.setTranslate(glm::vec3(0.25, 0.0, -0.25));
		colorVertexShader.use();
		colorVertexShader.setUniform4f("color", 0.5f, 0.5f, 0.2f, 1.0f);
		colorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(colorVertexIndexMesh.getGlobalTransform()));
		colorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		colorVertexIndexMesh.draw();

		texture0.apply(0);
		texture1.apply(1);
		texColorVertexMesh.setTranslate(glm::vec3(-0.25, 0.0, 0.25));
		texColorVertexShader.use();
		texColorVertexShader.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
		texColorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(texColorVertexMesh.getGlobalTransform()));
		texColorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		texColorVertexShader.setUniform1i("tex0", 0);
		texColorVertexShader.setUniform1i("tex1", 1);
		texColorVertexMesh.draw();

		texture0.apply(0);
		texture1.apply(1);
		texColorVertexIndexMesh.setTranslate(glm::vec3(0.25, 0.0, 0.25));
		texColorVertexShader.use();
		texColorVertexShader.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
		texColorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(texColorVertexIndexMesh.getGlobalTransform()));
		texColorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		texColorVertexShader.setUniform1i("tex0", 0);
		texColorVertexShader.setUniform1i("tex1", 1);
		texColorVertexIndexMesh.draw();

		texture0.apply(0);
		texture1.apply(1);
		texCubeColorVertexMesh.setTranslate(glm::vec3(-0.25, 0.0, 0.0));
		texCubeColorVertexShader.use();
		texCubeColorVertexShader.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
		texCubeColorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(texCubeColorVertexMesh.getGlobalTransform()));
		texCubeColorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		texCubeColorVertexShader.setUniform1i("tex0", 0);
		texCubeColorVertexShader.setUniform1i("tex1", 1);
		texCubeColorVertexMesh.draw();

		texture0.apply(0);
		texture1.apply(1);
		texCubeColorVertexIndexMesh.setTranslate(glm::vec3(0.25, 0.0, 0.0));
		texCubeColorVertexShader.use();
		texCubeColorVertexShader.setUniform4f("color", 1.0f, 1.0f, 1.0f, 1.0f);
		texCubeColorVertexShader.setUniformMatrix4fv("worldTransform", 1, glm::value_ptr(texCubeColorVertexIndexMesh.getGlobalTransform()));
		texCubeColorVertexShader.setUniformMatrix4fv("projViewTransform", 1, glm::value_ptr(camera.getProjectionViewTransform()));
		texCubeColorVertexShader.setUniform1i("tex0", 0);
		texCubeColorVertexShader.setUniform1i("tex1", 1);
		texCubeColorVertexIndexMesh.draw();
	}

	void DeInit()
	{
	}
private:
	bool InitCamera()
	{
		camera.setLookAt(campos, glm::vec3(0.0, 0, 0), glm::vec3(0, 1, 0));
		camera.setPerspective(90.0, ((float)SCR_WIDTH) / SCR_HEIGHT, 0.1, 1000);    // camera.setOrtho(-10, 10, -10, 10, 0.1, 1000);
		camera.setViewport(0, 0, 1, 1);

		return true;
	}

	bool InitTextures()
	{
		if(!texture0.construct("res/test1.jpg", Texture::RGB888))
			return false;
		if(!texture1.construct("res/test2.jpg", Texture::RGB888))
			return false;
		if(!textureCube0.construct("res/cubemap.jpg", Texture::RGB888))
			return false;

		return true;
	}

	bool InitShaders()
	{
		if (!vertexShader.loadSource("res/vertex.vs", "res/vertex.fs"))
			return false;
		if (!colorVertexShader.loadSource("res/colorVertex.vs", "res/colorVertex.fs"))
			return false;
		if (!texColorVertexShader.loadSource("res/texColorVertex.vs", "res/texColorVertex.fs"))
			return false;
		if (!texCubeColorVertexShader.loadSource("res/texColorVertex.vs", "res/texColorVertex.fs"))
			return false;

		return true;
	}

	bool InitMeshes()
	{
		///////////////////////////////////////////////////////////////
		{
			std::vector<Layout> meshLayout =
			{
				{ 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex) }
			};
			Vertex vertices[] =
			{
				{  0.25f, 0.0f,   0.25f },  // top right
				{  0.25f, 0.0f,  -0.25f },  // bottom right
				{ -0.25f, 0.0f,   0.25f },  // top left 
				{ -0.25f, 0.0f,   0.25f },  // top left 
				{  0.25f, 0.0f,  -0.25f },  // bottom right
				{ -0.25f, 0.0f,  -0.25f }   // bottom left
			};
			if (!vertexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(Vertex)))
				return false;
		}
		{
			std::vector<Layout> meshLayout =
			{
				{ 0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex) }
			};
			Vertex vertices[] =
			{
				{  +0.25f, 0.0f,  +0.25f },  // top right
				{  +0.25f, 0.0f,  -0.25f },  // bottom right
				{  -0.25f, 0.0f,  -0.25f },  // bottom left
				{  -0.25f, 0.0f,  +0.25f }   // top left 
			};
			unsigned int indices[] =
			{
				0, 1, 3,  // first Triangle
				1, 2, 3   // second Triangle
			};
			if(!vertexIndexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(Vertex), indices, sizeof(indices) / sizeof(unsigned int)))
				return false;
		}

		///////////////////////////////////////////////////////////////
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(ColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(ColorVertex) }
			};
			ColorVertex vertices[] =
			{
				{  +0.25f, 0.0f,  +0.25f, 1.0f, 0.0f, 0.0f, 1.0f },  // top right
				{  +0.25f, 0.0f,  -0.25f, 0.0f, 1.0f, 0.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  +0.25f, 0.0f, 0.0f, 1.0f, 1.0f },  // top left 
				{  -0.25f, 0.0f,  +0.25f, 0.0f, 0.0f, 1.0f, 1.0f },  // top left 
				{  +0.25f, 0.0f,  -0.25f, 0.0f, 1.0f, 0.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 0.0f, 1.0f }   // bottom left
			};
			if(!colorVertexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(ColorVertex)))
				return false;
		}
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(ColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(ColorVertex) }
			};
			ColorVertex vertices[] =
			{
				{  +0.25f, 0.0f,  +0.25f, 1.0f, 0.0f, 0.0f, 1.0f },  // top right
				{  +0.25f, 0.0f,  -0.25f, 0.0f, 1.0f, 0.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 0.0f, 1.0f },  // bottom left
				{  -0.25f, 0.0f,  +0.25f, 0.0f, 0.0f, 1.0f, 1.0f }   // top left 
			};
			unsigned int indices[] =
			{
				0, 1, 3,  // first Triangle
				1, 2, 3   // second Triangle
			};
			if(!colorVertexIndexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(ColorVertex), indices, sizeof(indices) / sizeof(unsigned int)))
				return false;
		}

		///////////////////////////////////////////////////////////////
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{2, 2, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) }
			};
			TexColorVertex vertices[] =
			{
				{  +0.25f, 0.0f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  +0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f, 0.0f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f }   // bottom left
			};
			if(!texColorVertexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(TexColorVertex)))
				return false;
		}
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{2, 2, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) }
			};
			TexColorVertex vertices[] =
			{
				{  +0.25f, 0.0f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },  // top right
				{  +0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, 0.0f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // bottom left
				{  -0.25f, 0.0f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f }   // top left 
			};
			unsigned int indices[] =
			{
				0, 1, 3,  // first Triangle
				1, 2, 3   // second Triangle
			};
			if(!texColorVertexIndexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(TexColorVertex), indices, sizeof(indices) / sizeof(unsigned int)))
				return false;
		}

		///////////////////////////////////////////////////////////////
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{2, 2, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) }
			};
			TexColorVertex vertices[] =
			{
				{  +0.25f, +0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  +0.25f, +0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, +0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f, +0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f, +0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, +0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },  // bottom left

				{  +0.25f, -0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  -0.25f, -0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f, -0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, -0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f, -0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },  // bottom left
				{  +0.25f, -0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right

				{  -0.25f,  +0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  -0.25f,  +0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f,  -0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f,  -0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f,  +0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f,  -0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },   // bottom left

				{  +0.25f,  +0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  +0.25f,  -0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f,  +0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  +0.25f,  -0.25f, +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f,  -0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },  // bottom left
				{  +0.25f,  +0.25f, -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right

				{  +0.25f, +0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  +0.25f, -0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, +0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f, +0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f, -0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, -0.25f, -0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },   // bottom left

				{  +0.25f, +0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },  // top right
				{  -0.25f, +0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  +0.25f, -0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
				{  -0.25f, +0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },  // top left 
				{  -0.25f, -0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 1.0f },  // bottom left
				{  +0.25f, -0.25f, +0.25f,  1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },  // bottom right
			};
			if(!texCubeColorVertexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(TexColorVertex)))
				return false;
		}

		///////////////////////////////////////////////////////////////
		{
			std::vector<Layout> meshLayout =
			{
				{0, 3, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{1, 4, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) },
				{2, 2, GL_FLOAT, GL_FALSE, sizeof(TexColorVertex) }
			};
			TexColorVertex vertices[] =
			{
				{  -0.25f, +0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },
				{  +0.25f, +0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },
				{  -0.25f, +0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },
				{  +0.25f, +0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },

				{  -0.25f, -0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f },
				{  +0.25f, -0.25f,  +0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f, 1.0f },
				{  -0.25f, -0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },
				{  +0.25f, -0.25f,  -0.25f, 1.0f, 1.0f, 1.0f, 1.0f, 0.0f, 0.0f },
			};

			unsigned int indices[] =
			{
				0, 1, 2,
				2, 1, 3,
				4, 6, 5,
				5, 6, 7,

				0, 2, 4,
				4, 2, 6,
				1, 5, 3,
				3, 5, 7,

				2, 3, 6,
				6, 3, 7,
				0, 4, 1,
				1, 4, 5
			};
			if(!texCubeColorVertexIndexMesh.construct(meshLayout, vertices, sizeof(vertices) / sizeof(TexColorVertex), indices, sizeof(indices) / sizeof(unsigned int)))
				return false;
		}

		return true;
	}

	void UpdateCamera(double deltaX, double deltaY, float dX, float dY, float dZ)
	{
		theta += deltaY * 180 / SCR_HEIGHT;
		if (theta > 150)
			theta = 150;
		else if (theta < 30)
			theta = 30;
		phi += deltaX * 2.0 * 180 / SCR_WIDTH;
		if (phi > 180)
			phi = 180;
		else if (phi < -180)
			phi = -180;

		campos.x = radius * sin(theta / 180 * 3.141592654) * cos(phi / 180 * 3.141592654);
		campos.z = radius * sin(theta / 180 * 3.141592654) * sin(phi / 180 * 3.141592654);
		campos.y = radius * cos(theta / 180 * 3.141592654);

		camera.setLookAt(campos, glm::vec3(0, 0, 0), glm::vec3(0, 1, 0));
	}

	struct Vertex
	{
		float x, y, z;
	};
	Shader vertexShader;
	Mesh<Vertex> vertexMesh;
	IndexMesh<Vertex, unsigned int> vertexIndexMesh;

	struct ColorVertex
	{
		float x, y, z;
		float r, g, b, a;
	};
	Shader colorVertexShader;
	Mesh<ColorVertex> colorVertexMesh;
	IndexMesh<ColorVertex, unsigned int> colorVertexIndexMesh;

	struct TexColorVertex
	{
		float x, y, z;
		float r, g, b, a;
		float u, v;
	};
	Shader texColorVertexShader;
	Mesh<TexColorVertex> texColorVertexMesh;
	IndexMesh<TexColorVertex, unsigned int> texColorVertexIndexMesh;

	Shader texCubeColorVertexShader;
	Mesh<TexColorVertex> texCubeColorVertexMesh;
	IndexMesh<TexColorVertex, unsigned int> texCubeColorVertexIndexMesh;

	Texture2D texture0;
	Texture2D texture1;
	TextureCube textureCube0;
	Camera camera;

	glm::vec3 campos = { 2.0f, 2.0f, 2.0f };
	float theta = 45;
	float phi = 45;
	float radius = 2.0f;
};

#endif