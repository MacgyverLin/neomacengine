#ifndef _Mesh_h_
#define _Mesh_h_

#include <vector>
#include <functional>
#include "Graphics.h"
#include "VertexAttribute.h"
#include "VertexBuffer.h"

template<class Vertex>
class Mesh : public Graphics3
{
public:
    Mesh()
        : vertexAttribute()
        , vertexBuffer()
    {
    }

    ~Mesh()
    {
        destruct();
    }

    bool construct(const std::vector<Layout>& layouts_, const Vertex* vertices_, int vertexCount_, int bufferType_ = GL_STATIC_DRAW)
    {
        destruct();

        return vertexAttribute.construct
        (
            layouts_,
            [&]() -> bool
            {
                return vertexBuffer.construct(vertices_, vertexCount_, bufferType_);
            }
        );
    }

    void destruct()
    {
        vertexAttribute.destruct();

        vertexBuffer.destruct();
    }

    void draw()
    {
        vertexAttribute.apply();

        vertexBuffer.draw(GL_TRIANGLES, 0, vertexBuffer.getPrimitiveCount());
    }
protected:
private:

public:
protected:
    VertexAttribute vertexAttribute;
    VertexBuffer<Vertex> vertexBuffer;
private:
};

#endif