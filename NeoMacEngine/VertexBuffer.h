#ifndef _VertexBuffer_h_
#define _VertexBuffer_h_

#include <vector>
#include <functional>
#include "Graphics.h"
#include "VertexAttribute.h"

template<class Vertex>
class VertexBuffer
{
public:
    VertexBuffer()
        : vbo(0)
        , count(0)
    {
    }

    ~VertexBuffer()
    {
        destruct();
    }

    bool construct(const Vertex* vertices_, int vertexCount_, int bufferType_ = GL_STATIC_DRAW)
    {
        destruct();

        glGenBuffers(1, &vbo);
        if (vbo)
        {
            glBindBuffer(GL_ARRAY_BUFFER, vbo);
            glBufferData(GL_ARRAY_BUFFER, sizeof(Vertex) * vertexCount_, vertices_, bufferType_);

            count = vertexCount_;
            return true;
        }
        else
        {
            return false;
        }
    }

    void destruct()
    {
        if (vbo)
        {
            glDeleteBuffers(1, &vbo);
            vbo = 0;
        }

        count = 0;
    }

    unsigned int getPrimitiveCount() const
    {
        return count;
    }

    void draw(GLenum mode, unsigned int start, unsigned int count)
    {
        glDrawArrays(mode, start, count);
    }
protected:
private:


public:
protected:
private:
    GLuint vbo;
    int count;
};

#endif