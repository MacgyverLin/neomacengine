#ifndef _Graphics_h_
#define _Graphics_h_

const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

#include <glad/glad.h>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/mat4x4.hpp>
#include <vector>

class Frame3
{
public:
    Frame3()
       : localTransform(glm::mat4(1.0f))
       , globalTransform(glm::mat4(1.0f))
       , globalTransformValid(false)
       , parent(nullptr)
       , children()
    {
    }

    ~Frame3()
    {
    }

    void addChild(Frame3* child_)
    {
        if (child_->parent)
        {
            child_->parent->removeChild(this);
        }

        children.push_back(child_);
    }

    void insertChild(int i_, Frame3* child_)
    {
        if (child_->parent)
        {
            child_->parent->removeChild(this);
        }

        children.push_back(child_);
    }

    void removeChild(Frame3* child_)
    {
        std::vector<Frame3*>::iterator itr = children.end();
        for (itr = children.begin(); itr != children.end(); itr++)
        {
            if (*itr == child_)
                break;            
        }
        
        if (itr != children.end())
        {
            children.erase(itr);
            child_->parent = nullptr;
        }
    }

    Frame3* getChild(int i_)
    {
        assert(i_ >= 0 && i_ < children.size());

        return children[i_];
    }

    const Frame3* getChild(int i_) const
    {
        assert(i_ >= 0 && i_ < children.size());

        return children[i_];
    }

    Frame3* getParent()
    {
        return parent;
    }

    const Frame3* getParent() const
    {
        return parent;
    }

    size_t getChildCount() const
    {
        return children.size();
    }

    void setLocalTransform(const glm::mat4x4& transform_)
    {
        localTransform = transform_;
        
        globalTransformValid = false;
    }

    void setGlobalTransform(const glm::mat4x4& transform_)
    {
        if (parent)
        {
            localTransform = getGlobalTransformInverse() * transform_;
        }
        else
        {
            localTransform = transform_;
        }

        globalTransformValid = false;
    }

    void setLocalXAxis(const glm::vec3& axis)
    {
        localTransform[0][0] = axis[0];
        localTransform[0][1] = axis[1];
        localTransform[0][2] = axis[2];

        globalTransformValid = false;
    }

    void setLocalYAxis(const glm::vec3& axis)
    {
        localTransform[1][0] = axis[0];
        localTransform[1][1] = axis[1];
        localTransform[1][2] = axis[2];

        globalTransformValid = false;
    }

    void setLocalZAxis(const glm::vec3& axis)
    {
        localTransform[2][0] = axis[0];
        localTransform[2][1] = axis[1];
        localTransform[2][2] = axis[2];

        globalTransformValid = false;
    }

    void setLocalPosition(const glm::vec3& pos)
    {
        localTransform[3][0] = pos[0];
        localTransform[3][1] = pos[1];
        localTransform[3][2] = pos[2];

        globalTransformValid = false;
    }

    glm::vec3 calcLocalAxis(const glm::vec3& axis)
    {
        glm::vec3 localAxis;
        if (parent)
        {
            //            localAxis = getGlobalTransformInverse() * axis;
            localAxis = axis;
        }
        else
        {
            localAxis = axis;
        }

        return localAxis;
    }

    glm::vec3 calcLocalPosition(const glm::vec3& pos)
    {
        glm::vec3 localPos;
        if (parent)
        {
            // localPos = getGlobalTransformInverse() * axis;
            localPos = pos;
        }
        else
        {
            localPos = pos;
        }

        return localPos;
    }

    void setGlobalXAxis(const glm::vec3& axis)
    {
        glm::vec3 localAxis = calcLocalAxis(axis);

        localTransform[0][0] = localAxis[0];
        localTransform[0][1] = localAxis[1];
        localTransform[0][2] = localAxis[2];

        globalTransformValid = false;
    }

    void setGlobalYAxis(const glm::vec3& axis)
    {
        glm::vec3 localAxis = calcLocalAxis(axis);

        localTransform[1][0] = localAxis[0];
        localTransform[1][1] = localAxis[1];
        localTransform[1][2] = localAxis[2];

        globalTransformValid = false;
    }

    void setGlobalZAxis(const glm::vec3& axis)
    {
        glm::vec3 localAxis = calcLocalAxis(axis);

        localTransform[2][0] = localAxis[0];
        localTransform[2][1] = localAxis[1];
        localTransform[2][2] = localAxis[2];

        globalTransformValid = false;
    }

    void setGlobalPosition(const glm::vec3& pos)
    {
        glm::vec3 localAxis = calcLocalPosition(pos);

        localTransform[3][0] = localAxis[0];
        localTransform[3][1] = localAxis[1];
        localTransform[3][2] = localAxis[2];

        globalTransformValid = false;
    }

    ////////////////////////////////////////////////////
    const glm::mat4x4& getLocalTransform()
    {
        return localTransform;
    }

    const glm::mat4x4& getGlobalTransform()
    {
        validateGlobalTransform();

        return globalTransform;
    }

    const glm::mat4x4& getGlobalTransformInverse()
    {
        validateGlobalTransformAndInverse();

        return globalTransformInverse;
    }

    glm::vec3 getLocalXAxis()
    {
        return glm::vec3(localTransform[0][0], localTransform[0][1], localTransform[0][2]);
    }

    glm::vec3 getLocalYAxis()
    {
        return glm::vec3(localTransform[1][0], localTransform[1][1], localTransform[1][2]);
    }

    glm::vec3 getLocalZAxis()
    {
        return glm::vec3(localTransform[2][0], localTransform[2][1], localTransform[2][2]);
    }

    glm::vec3 getLocalPosition()
    {
        return glm::vec3(localTransform[3][0], localTransform[3][1], localTransform[3][2]);
    }

    glm::vec3 getGlobalXAxis()
    {
        validateGlobalTransform();

        return glm::vec3(globalTransform[0][0], globalTransform[0][1], globalTransform[0][2]);
    }

    glm::vec3 getGlobalYAxis()
    {
        validateGlobalTransform();

        return glm::vec3(globalTransform[1][0], globalTransform[1][1], globalTransform[1][2]);
    }

    glm::vec3 getGlobalZAxis()
    {
        validateGlobalTransform();

        return glm::vec3(globalTransform[2][0], globalTransform[2][1], globalTransform[2][2]);
    }

    glm::vec3 getGlobalPosition()
    {
        validateGlobalTransform();

        return glm::vec3(globalTransform[3][0], globalTransform[3][1], globalTransform[3][2]);
    }

    ////////////////////////////////////////////////////
    void setIdentity(bool local = true)
    {
        if(local)
            setLocalTransform(glm::mat4x4(1.0f));
        else
            setGlobalTransform(glm::mat4x4(1.0f));
    }

    void setTranslate(const glm::vec3& t, bool local = true)
    {
        if (local)
            setLocalTransform(glm::translate(glm::mat4x4(1.0f), t));
        else
            setGlobalTransform(glm::translate(glm::mat4x4(1.0f), t));
    }

    void setRotate(float angle, const glm::vec3& axis, bool local = true)
    {
        if (local)
            setLocalTransform(glm::rotate(glm::mat4x4(1.0f), glm::radians(angle), axis));
        else
            setGlobalTransform(glm::rotate(glm::mat4x4(1.0f), glm::radians(angle), axis));
    }

    void setScale(const glm::vec3& s, bool local = true)
    {
        if (local)
            setLocalTransform(glm::scale(glm::mat4x4(1.0f), s));
        else
            setGlobalTransform(glm::scale(glm::mat4x4(1.0f), s));
    }

    void setLookAt(const glm::vec3& eye, const glm::vec3& target, const glm::vec3& up, bool local = true)
    {
        if (local)
            setLocalTransform(glm::inverse(glm::lookAt(eye, target, up)));
        else
            setGlobalTransform(glm::inverse(glm::lookAt(eye, target, up)));
    }
protected:
    void validateGlobalTransform()
    {
        if (!globalTransformValid)
        {
            if (parent)
                globalTransform = parent->getGlobalTransform() * localTransform;
            else
                globalTransform = localTransform;

            globalTransformValid = true;
        }
    }

    void validateGlobalTransformAndInverse()
    {
        if (!globalTransformValid)
        {
            validateGlobalTransform();

            globalTransformInverse = glm::inverse(globalTransform);
        }
    }

    bool isGlobalTransformValid() const
    {
        return globalTransformValid;
    }
private:

public:
protected:
private:
    glm::mat4x4 localTransform;
    glm::mat4x4 globalTransform;
    glm::mat4x4 globalTransformInverse;
    bool globalTransformValid;

    Frame3* parent;
    std::vector<Frame3*> children;
};

class Graphics3 : public Frame3
{
public:
    Graphics3()
    {
    }

    ~Graphics3()
    {
    }
};

#endif