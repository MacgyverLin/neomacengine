#ifndef _IndexMesh_h_
#define _IndexMesh_h_

#include <vector>
#include <functional>
#include "Graphics.h"
#include "VertexAttribute.h"
#include "VertexBuffer.h"
#include "IndexBuffer.h"

template<class Vertex, class Index>
class IndexMesh : public Graphics3
{
public:
    IndexMesh()
        : vertexAttribute()
        , vertexBuffer()
        , indexBuffer()
    {
    }

    ~IndexMesh()
    {
        destruct();
    }

    bool construct(const std::vector<Layout>& layouts_, const Vertex* vertices_, int vertexCount_, const Index* indices_, int indexCount_, int bufferType_ = GL_STATIC_DRAW)
    {
        destruct();

        return vertexAttribute.construct
        (
            layouts_,
            [&]() -> bool
            {
                if (vertexBuffer.construct(vertices_, vertexCount_, bufferType_) && indexBuffer.construct(indices_, indexCount_, bufferType_))
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        );
    }

    void destruct()
    {
        vertexAttribute.destruct();

        vertexBuffer.destruct();

        indexBuffer.destruct();
    }

    void draw()
    {
        vertexAttribute.apply();

        indexBuffer.draw(GL_TRIANGLES, 0, indexBuffer.getPrimitiveCount());
    }
protected:
private:

public:
protected:
    VertexAttribute vertexAttribute;
    VertexBuffer<Vertex> vertexBuffer;
    IndexBuffer<Index> indexBuffer;
private:
};

#endif