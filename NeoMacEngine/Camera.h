#ifndef _Camera_h_
#define _Camera_h_

#include <vector>
#include <functional>
#include "Graphics.h"

class Camera : public Graphics3
{
public:
    Camera()
        : projectionViewValid(false)
    {
    }

    ~Camera()
    {
        destruct();
    }

    bool construct()
    {
        destruct();
    }

    void destruct()
    {
    }

    void setViewport(float x, float y, float w, float h)
    {
        viewport.x = x;
        viewport.y = y;
        viewport.z = w;
        viewport.w = h;
    }

    const glm::vec4& getViewport(float x, float y, float w, float h) const
    {
        return viewport;
    }

    void setPerspective(float fovy, float aspect, float zNear, float zFar)
    {
        projectionTransform = glm::perspective(fovy / 180.0f * 3.141592654f, aspect, zNear, zFar);

        projectionViewValid = false;
    }

    void setFrustum(float left, float right, float bottom, float top, float nearVal, float farVal)
    {
        projectionTransform = glm::frustum(left, right, bottom, top, nearVal, farVal);

        projectionViewValid = false;
    }

    void setOrtho(float left, float right, float bottom, float top, float nearVal, float farVal)
    {
        projectionTransform = glm::ortho(left, right, bottom, top, nearVal, farVal);

        projectionViewValid = false;
    }

    const glm::mat4x4& getViewTransform()
    {
        return getGlobalTransformInverse();
    }

    const glm::mat4x4& getProjectionTransform()
    {
        return projectionTransform;
    }

    void validateProjectionViewTransform()
    {
        if (!isGlobalTransformValid() || !projectionViewValid)
        {
            const glm::mat4x4& view = getViewTransform();
            const glm::mat4x4& proj = getProjectionTransform();
            projectionViewTransform = proj * view;
            
            projectionViewValid = true;
        }
    }

    const glm::mat4x4& getProjectionViewTransform()
    {
        validateProjectionViewTransform();

        return projectionViewTransform;
    }

    void project(glm::vec3& p)
    {
        glm::project(p, getViewTransform(), projectionTransform, viewport);
    }

    void unproject(glm::vec3& p)
    {
        glm::unProject(p, getViewTransform(), projectionTransform, viewport);
    }

    void draw()
    {
        glEnable(GL_SCISSOR_TEST);
        glScissor(viewport.x, viewport.y, viewport.z * SCR_WIDTH, viewport.w * SCR_HEIGHT);
        glViewport(viewport.x, viewport.y, viewport.z * SCR_WIDTH, viewport.w * SCR_HEIGHT);

        glClearColor(0.0f, 0.0f, 0.0f, 1.0f);
        glClearDepth(1.0f);
        glClearStencil(255);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
    }
protected:
private:


public:
protected:
private:
    glm::vec4 viewport;
    glm::mat4x4 projectionTransform;

    bool projectionViewValid;
    glm::mat4x4 projectionViewTransform;
};

#endif