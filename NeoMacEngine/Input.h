#ifndef _Input_h_
#define _Input_h_

class Input
{
public:
	static double mouseX;
	static double mouseY;
	static float dx;
	static float dy;
	static float dz;
};

#endif