#ifndef _TextureCube_h_
#define _TextureCube_h_

#include "Texture.h"

class TextureCube : public Texture
{
public:
    TextureCube() : Texture(GL_TEXTURE_CUBE_MAP)
    {
        setWrapS(GL_CLAMP_TO_EDGE);
        setWrapR(GL_CLAMP_TO_EDGE);
        setWrapT(GL_CLAMP_TO_EDGE);
    }

    ~TextureCube()
    {
        destruct();
    }
protected:
    unsigned int getPixelSize(GLenum format, GLenum type)
    {
        unsigned int dataSize = 0;
        switch (type)
        {
        case GL_UNSIGNED_SHORT_5_6_5:
            dataSize = 2;
            break;
        case GL_UNSIGNED_SHORT_4_4_4_4:
            dataSize = 2;
            break;
        case GL_UNSIGNED_SHORT_5_5_5_1:
            dataSize = 2;
            break;
        case GL_UNSIGNED_BYTE:
        default:
            dataSize = 1;
            break;
        };

        switch (format)
        {
            case GL_ALPHA:
                dataSize *= 1;
                break;
            case GL_RGB:
                dataSize *= 3;
                break;
            default:
            case GL_RGBA:
                dataSize *= 4;
                break;
            case GL_LUMINANCE:
                dataSize *= 1;
                break;
            case GL_LUMINANCE_ALPHA:
                dataSize *= 2;
                break;
        };

        return dataSize;
    }

    virtual void setTexImage(GLenum target,
        GLint level,
        GLint internalFormat,
        GLsizei width,
        GLsizei height,
        GLint border,
        GLenum format,
        GLenum type,
        const GLvoid* data)
    {
        unsigned int imageSize = getPixelSize(format, type) * width * height;
        const GLbyte* byte = (const GLbyte*)data;
        unsigned int offset = 0;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_X, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Y, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Y, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_Z, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
        glTexImage2D(GL_TEXTURE_CUBE_MAP_NEGATIVE_Z, level, internalFormat, width, height, border, format, type, &byte[offset]); offset += imageSize;
    }
private:

public:
protected:
private:
};

#endif