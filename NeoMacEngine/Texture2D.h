#ifndef _Texture2D_h_
#define _Texture2D_h_

#include "Texture.h"

class Texture2D : public Texture
{
public:
    Texture2D() : Texture(GL_TEXTURE_2D)
    {
    }

    ~Texture2D()
    {
        destruct();
    }
protected:
    virtual void setTexImage(GLenum target,
        GLint level,
        GLint internalFormat,
        GLsizei width,
        GLsizei height,
        GLint border,
        GLenum format,
        GLenum type,
        const GLvoid* data)
    {
        glTexImage2D(target, level, internalFormat, width, height, border, format, type, data);
    }
private:

public:
protected:
private:
};

#endif