#ifndef _Shader_h_
#define _Shader_h_

#include <glad/glad.h>
#include <map>
#include <fstream>
#include <sstream>

class Shader
{
public:
	Shader()
		: shaderProgram(0)
	{
	}

	~Shader()
	{
		if (shaderProgram)
		{
			glDeleteProgram(shaderProgram);
			shaderProgram = 0;
		}
	}

	bool loadSource(const char* vertexShaderPath, const char* fragmentShaderPath)
	{
		return loadSource(vertexShaderPath, nullptr, nullptr, fragmentShaderPath);
	}

	bool loadSource(const char* vertexShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath)
	{
		return loadSource(vertexShaderPath, nullptr, geometryShaderPath, fragmentShaderPath);
	}

	bool loadSource(const char* vertexShaderPath, const char* tesellationShaderPath, const char* geometryShaderPath, const char* fragmentShaderPath)
	{
		std::string buf0;
		std::string buf1;
		std::string buf2;
		std::string buf3;
		const char* vertexShaderSource = loadSourceFile(vertexShaderPath, buf0);
		const char* tesellationShaderSource = loadSourceFile(tesellationShaderPath, buf1);
		const char* geometryShaderSource = loadSourceFile(geometryShaderPath, buf2);
		const char* fragmentShaderSource = loadSourceFile(fragmentShaderPath, buf3);

		return setSource(vertexShaderSource, tesellationShaderSource, geometryShaderSource, fragmentShaderSource);
	}

	bool setSource(const char* vertexShaderSource, const char* fragmentShaderSource)
	{
		return setSource(vertexShaderSource, nullptr, nullptr, fragmentShaderSource);
	}

	bool setSource(const char* vertexShaderSource, const char* geometryShaderSource, const char* fragmentShaderSource)
	{
		return setSource(vertexShaderSource, nullptr, geometryShaderSource, fragmentShaderSource);
	}

	bool setSource(const char* vertexShaderSource, const char* tesellationShaderSource, const char* geometryShaderSource, const char* fragmentShaderSource)
	{
		unsigned int vertexShader = 0;
		unsigned int tesellationShader = 0;
		unsigned int geometryShader = 0;
		unsigned int fragmentShader = 0;

		if (vertexShaderSource)
		{
			vertexShader = compileShader(vertexShaderSource, GL_VERTEX_SHADER);
		}

		//if (tesellationShaderSource)
		//{
			//tesellationShader = compileShader(tesellationShaderSource, GL_COMPUTE_SHADER);
		//}

		if (geometryShaderSource)
		{
			geometryShader = compileShader(geometryShaderSource, GL_GEOMETRY_SHADER);
		}

		if (fragmentShaderSource)
		{
			fragmentShader = compileShader(fragmentShaderSource, GL_FRAGMENT_SHADER);
		}

		shaderProgram = glCreateProgram();
		if (vertexShader)
			glAttachShader(shaderProgram, vertexShader);
		if (tesellationShader)
			glAttachShader(shaderProgram, tesellationShader);
		if (geometryShader)
			glAttachShader(shaderProgram, geometryShader);
		if (fragmentShader)
			glAttachShader(shaderProgram, fragmentShader);

		glLinkProgram(shaderProgram);

		if (vertexShader)
		{
			glDeleteShader(vertexShader);
		}
		if (tesellationShader)
		{
			glDeleteShader(tesellationShader);
		}
		if (geometryShader)
		{
			glDeleteShader(geometryShader);
		}
		if (fragmentShader)
		{
			glDeleteShader(fragmentShader);
		}

		// vecn: the default vector of n floats.
		// bvecn : a vector of n booleans.
		// ivecn : a vector of n integers.
		// uvecn : a vector of n unsigned integers.
		// dvecn : a vector of n double components.
		int nrAttributes;
		glGetIntegerv(GL_MAX_VERTEX_ATTRIBS, &nrAttributes);
		std::cout << "Maximum num of vertex attributes supported: " << nrAttributes << std::endl;

		int numActiveUniform = 0;
		glGetProgramiv(shaderProgram, GL_ACTIVE_UNIFORMS, &numActiveUniform);
		for (int i = 0; i < numActiveUniform; i++)
		{
			GLsizei length;
			GLint size;
			GLenum type;
			GLchar name[512];

			glGetActiveUniform(shaderProgram, i, 512, &length, &size, &type, name);
			string2Location[name] = glGetUniformLocation(shaderProgram, name);
		}

		return true;
	}

	void setSource(const char* computeShaderSource)
	{
		unsigned int computeShader = 0;

		if (computeShaderSource)
		{
			computeShader = compileShader(computeShaderSource, GL_COMPUTE_SHADER);
		}

		shaderProgram = glCreateProgram();
		if (computeShader)
		{
			glAttachShader(shaderProgram, computeShader);
		}

		glLinkProgram(shaderProgram);

		if (computeShader)
		{
			glDeleteShader(computeShader);
		}

		int numActiveUniform = 0;
		glGetProgramiv(shaderProgram, GL_ACTIVE_UNIFORMS, &numActiveUniform);
		for (int i = 0; i < numActiveUniform; i++)
		{
			GLsizei length;
			GLint size;
			GLenum type;
			GLchar name[512];

			glGetActiveUniform(shaderProgram, i, 512, &length, &size, &type, name);
			string2Location[name] = glGetUniformLocation(shaderProgram, name);
		}
	}

	void setUniform1f(GLint location, GLfloat v0)
	{
		glUniform1f(location, v0);
	}

	void setUniform2f(GLint location, GLfloat v0, GLfloat v1)
	{
		glUniform2f(location, v0, v1);
	}

	void setUniform3f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2)
	{
		glUniform3f(location, v0, v1, v2);
	}

	void setUniform4f(GLint location, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
	{
		glUniform4f(location, v0, v1, v2, v3);
	}

	void setUniform1i(GLint location, GLint v0)
	{
		glUniform1i(location, v0);
	}

	void setUniform2i(GLint location, GLint v0, GLint v1)
	{
		glUniform2i(location, v0, v1);
	}

	void setUniform3i(GLint location, GLint v0, GLint v1, GLint v2)
	{
		glUniform3i(location, v0, v1, v2);
	}

	void setUniform4i(GLint location, GLint v0, GLint v1, GLint v2, GLint v3)
	{
		glUniform4i(location, v0, v1, v2, v3);
	}

	void setUniform1fv(GLint location, GLsizei count, const GLfloat* value)
	{
		glUniform1fv(location, count, value);
	}

	void setUniform2fv(GLint location, GLsizei count, const GLfloat* value)
	{
		glUniform2fv(location, count, value);
	}

	void setUniform3fv(GLint location, GLsizei count, const GLfloat* value)
	{
		glUniform3fv(location, count, value);
	}

	void setUniform4fv(GLint location, GLsizei count, const GLfloat* value)
	{
		glUniform4fv(location, count, value);
	}

	void setUniform1iv(GLint location, GLsizei count, const GLint* value)
	{
		glUniform1iv(location, count, value);
	}

	void setUniform2iv(GLint location, GLsizei count, const GLint* value)
	{
		glUniform2iv(location, count, value);
	}

	void setUniform3iv(GLint location, GLsizei count, const GLint* value)
	{
		glUniform3iv(location, count, value);
	}

	void setUniform4iv(GLint location, GLsizei count, const GLint* value)
	{
		glUniform4iv(location, count, value);
	}

	void setUniformMatrix2fv(GLint location,
		GLsizei count,
		const GLfloat* value)
	{
		glUniformMatrix2fv(location, count, GL_FALSE, value);
	}

	void setUniformMatrix3fv(GLint location,
		GLsizei count,
		const GLfloat* value)
	{
		glUniformMatrix3fv(location, count, GL_FALSE, value);
	}

	void setUniformMatrix4fv(GLint location, GLsizei count, const GLfloat* value)
	{
		glUniformMatrix4fv(location, count, GL_FALSE, value);
	}

	void setUniform1f(const char* name, GLfloat v0)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform1f(location, v0);
	}

	void setUniform2f(const char* name, GLfloat v0, GLfloat v1)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform2f(location, v0, v1);
	}

	void setUniform3f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform3f(location, v0, v1, v2);
	}

	void setUniform4f(const char* name, GLfloat v0, GLfloat v1, GLfloat v2, GLfloat v3)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform4f(location, v0, v1, v2, v3);
	}

	void setUniform1i(const char* name, GLint v0)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform1i(location, v0);
	}

	void setUniform2i(const char* name, GLint v0, GLint v1)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform2i(location, v0, v1);
	}

	void setUniform3i(const char* name, GLint v0, GLint v1, GLint v2)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform3i(location, v0, v1, v2);
	}

	void setUniform4i(const char* name, GLint v0, GLint v1, GLint v2, GLint v3)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform4i(location, v0, v1, v2, v3);
	}

	void setUniform1fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform1fv(location, count, value);
	}

	void setUniform2fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform2fv(location, count, value);
	}

	void setUniform3fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform3fv(location, count, value);
	}

	void setUniform4fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform4fv(location, count, value);
	}

	void setUniform1iv(const char* name, GLsizei count, const GLint* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform1iv(location, count, value);
	}

	void setUniform2iv(const char* name, GLsizei count, const GLint* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform2iv(location, count, value);
	}

	void setUniform3iv(const char* name, GLsizei count, const GLint* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform3iv(location, count, value);
	}

	void setUniform4iv(const char* name, GLsizei count, const GLint* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniform4iv(location, count, value);
	}

	void setUniformMatrix2fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniformMatrix2fv(location, count, GL_FALSE, value);
	}

	void setUniformMatrix3fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniformMatrix3fv(location, count, GL_FALSE, value);
	}

	void setUniformMatrix4fv(const char* name, GLsizei count, const GLfloat* value)
	{
		int location = getUniformLocation(name);
		if (location == -1)
			return;

		glUniformMatrix4fv(location, count, GL_FALSE, value);
	}

	int getUniformLocation(const char* name)
	{
		if (string2Location.find(name) == string2Location.end())
			return -1;

		return string2Location[name];
	}

	void use() const
	{
		if (shaderProgram)
		{
			glUseProgram(shaderProgram);
		}
	}
protected:
	unsigned int compileShader(const char* source, int type) const
	{
		unsigned int shader = glCreateShader(type);
		glShaderSource(shader, 1, &source, NULL);
		glCompileShader(shader);

		int  success;
		char infoLog[512];
		glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
		if (!success)
		{
			glGetShaderInfoLog(shader, 512, NULL, infoLog);
			std::cout << "ERROR::SHADER::VERTEX::COMPILATION_FAILED\n" << infoLog << std::endl;
		}

		return shader;
	}

	const char* loadSourceFile(const char* path, std::string& code)
	{
		if (!path)
			return nullptr;

		std::ifstream shaderFile;
		shaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit); // ensure ifstream objects can throw exceptions:
		try
		{
			shaderFile.open(path); // open files

			std::stringstream shaderStream;
			shaderStream << shaderFile.rdbuf(); // read file's buffer contents into streams
			shaderFile.close(); // close file handlers

			code = shaderStream.str(); // convert stream into string
		}
		catch (std::ifstream::failure e)
		{
			std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
			return nullptr;
		}

		return code.c_str();
	}
private:

public:
protected:
private:
	unsigned int shaderProgram;
	std::map<std::string, unsigned int> string2Location;
};

#endif