#version 330 core
in vec4 vColor;
in vec2 vTexCoord0;
out vec4 FragColor;

uniform vec4 color;
uniform sampler2D tex0;

void main()
{
    FragColor = vColor * color * texture(tex0, vTexCoord0);
}