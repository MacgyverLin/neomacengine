#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec4 aColor;
out vec4 vColor;

uniform mat4 worldTransform;
uniform mat4 projViewTransform;

void main()
{
    gl_Position = projViewTransform * worldTransform * vec4(aPos.x, aPos.y, aPos.z, 1.0);
    vColor = aColor;
}